import java.util.List;
public class FP_Funtional_01 {
    public static void main (String[] args){
         List<Integer> numbers = List.of(12, 9, 13, 4, 6, 2, 4, 12, 15);
         System.out.println("Using to --> [System.out::print] by default");
         printAllNumbersInListFunctional_Two(numbers);
         System.out.println("\n printAllNumbersInListFunctional: ");
         printAllNumbersInListFunctional(numbers);
         System.out.println("\n printEvenNumbersInListFunctional: ");
         printEvenNumbersInListFunctional(numbers);
         System.out.println("\n printSquaresOfEvenNumbersInListFunctional: ");
         printSquarestOfEvenNumbersInListFunctional(numbers);
         System.out.println("");
    }
    
    private static void print(int number){
        System.out.println(number+ ", ");
    }
    
    private static boolean isEven(int number){
        return (number % 2 == 0);
    }
    
    private static void printAllNumbersInListFunctional_Two(List<Integer> numbers){
        numbers.stream()                        //----> Convert to stream
                .forEach(System.out::print);    //---->Method Reference
                System.out.println("");
    }
    
    private static void printAllNumbersInListFunctional(List<Integer> numbers){
        numbers.stream()
                .forEach(FP_Funtional_01::print);
                System.out.println("");
    }
    
    private static void printEvenNumbersInListFunctional(List<Integer> numbers){
        numbers.stream()
                .filter(FP_Funtional_01::isEven)
                .forEach(FP_Funtional_01::print);
        System.out.println("");
    }
    
    private static void printSquarestOfEvenNumbersInListFunctional(List<Integer> numbers){
        numbers.stream()
                .filter(number -> number % 2 == 0)
                .map(number -> number * number)
                .forEach(FP_Funtional_01::print);
        System.out.println("");
    }
}
